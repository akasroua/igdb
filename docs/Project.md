IGDB: Base de datos internacional de glaciares
==============================================

Descripción del problema
------------------------

Pretendemos crear una base de datos que incluye información general y
específica de los principales glaciares. Nuestro fin es compartir
información con la comunidad científica, sin ánimo de lucro, y
concienciar a un número máximo de personas sobre los devastadores
efectos del cambio climático.

La IGDB ofrecerá dos interfaces distintas para la consulta de
información, una con información extensiva enfocada para el uso
científico (se podrán incluir los datos en cualquier documento
científico, mencionándola en la bibliografía), y otra con información
general y mucho más visual para el público general. Para el uso
científico, ofreceremos estadísticas individuales de un glaciar y la
posibilidad de comparar dos glaciares para observar la correlación entre
sus tasas de cambio. Para el público general, añadiremos un conjunto de
tablas y gráficas, junto con valores estadísticos que permiten
visualizar el impacto del cambio climático de forma muy intuitiva.

Nos basaremos en la base de datos @wgms-db, ya que ésta contiene una
mucha información acerca de gran número de glaciares. IGDB simplificará
la WGMS, escogiendo únicamente los datos relevantes para estudios acerca
del cambio climático, y acotando éstos a la década actual (2010-2018).

Requisitos
----------

### Datos

1.  **RD1**: Datos del glaciar
    -   País - *Cadena de 60 caracteres máximo*
    -   Nombre del glaciar - *Cadena de 60 caracteres máximo*
    -   ID del glaciar (Compatible con la WGMS) - *Cadena de 20
        caracteres*
2.  **RD2**: Datos anuales de un glaciar
    -   ID del glaciar (Compatible con la WGMS) - *Cadena de 20
        caracteres*
    -   Área - *Decimal*
    -   Volumen - *Decimal*
    -   Altura - *Decimal*
    -   Año - *Entero de 11 dígitos*
3.  **RD3**: Datos del administrador
    -   ID - *Entero de 11 dígitos*
    -   Fecha y hora de alta - *Fecha y hora en formato yyyy-mm-dd
        hh:mm*
    -   Nombre de usuario - *Cadena de 20 caracteres máximo*
    -   Hash de la contraseña - *Cadena de 128 caracteres máximo*

### Funcionales

1.  **RF1**: Alta de un glaciar

    Se añade un glaciar al sistema a partir de los datos de la WGMS

    -   Entrada: **RD1**

2.  **RF2**: Inclusión de datos anuales

    Añade los datos relevantes de cada año para un glaciar

    -   Entrada: **RD2**

3.  **RF3**: Cálculo de las variaciones anuales

    Calcula las variaciones anuales de altura, área y longitud para un
    glaciar

    -   Entrada: **RD2**
    -   Manejo: **RD3**

4.  **RF4**: Alta del administrador

    Crea un usuario de tipo administrador, encargado de monitorizar el
    sistema y resolver conflictos

    -   Entrada: **RD4**

5.  **RF5**: Actualización de la base de datos

    Añade nuevos datos a partir de nuevas versiones de la WGMS

    -   Entrada: **RD2**

6.  **RF6**: Resolución de conflictos

    Permite al administrador la resolución de conflictos de datos al
    actualizar la base de datos

### No Funcionales

1.  **RNF1**: Seguridad

    La página web de consulta será accesible únicamente mediante HTTPS,
    y la base de datos tendrá su propio usuario de acceso

2.  **RNF2**: Escalabilidad

    Se podrá aumentar el rendimiento de IGDB mediante una mejora del
    hardware del servidor, o montando un cluster

3.  **RNF3**: Disponibilidad

    La IGDB estará disponible 24/7, y en caso de necesidad de
    mantenimiento, se procederá a mostrar un snapshot de la página

4.  **RNF4**: Tolerancia a fallos

    Se usará un cluster para permitir que la IGDB siga siendo
    disponible, aunque falle algún servidor

5.  **RNF5**: Copias de seguridad

    Se harán copias de seguridad diarias del sistema, además de
    enviarlas a otro servidor en caso de que se pierdan los datos
    locales de backup

6.  **RNF6**: Rotación de logs

    Se eliminarán los logs del sistema antiguos, cada semana

### Restricciones Semánticas

1.  **RS1**: No podrá haber dos glaciares con el mismo ID

2.  **RS2**: No podrá haber más de un administrador del sistema

3.  **RS3**: El atributo año solo podrá estar comprendido entre
    2010-2018, inclusive

Diagramas
---------

Para el diseño del sistema, procederemos con la modelización de
diagramas estandarizados, dado que es una herramienta imprescindible en
Ingeniería del Software.

### Diagrama funcional

![](./FD.png)

### Diagramas de flujo

![](./DF1.png)

![](./DF2.png)

![](./DF3.png)

![](./DF4.png)

![](./DF5.png)

![](./DF6.png)

### Diagrama de caja negra

![IGDB](./BB.png)

\clearpage

### Diagrama Entidad-Relación

![](./ER.png)

Bibliografía
------------
