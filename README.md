IGDB: Internation Glacier Database
==================================

The IGDB is a database, that uses data from the
[WGMS](https://dx.doi.org/10.5904/wgms-fog-2019-12) to illustrate the
consequences of climate change.

Our system allows you to visualize data with tables and plots, via our
intuitive Web UI.

![Table](./assets/screenshots/Table.png)

![Plot](./assets/screenshots/Plot.png)

Technologies used
-----------------

-   Flask
-   SQLAlchemy
-   Pandas

Requirements
------------

-   Python3
-   POSIX shell
-   Pip
-   Pipenv
-   MySQL/MariaDB

Installation
------------

1.  Clone the repository:

``` bash
git clone https://coolneng.duckdns.org/gitea/coolneng/igdb
```

2.  Change the working directory to the project:

``` bash
cd igdb/code
```

3.  Install the dependencies:

``` bash
pipenv install
```

All the dependencies will be installed inside a virtualenv.

Usage
-----

1.  Start a shell inside the virtualenv:

``` bash
pipenv shell
```

2.  Start the Flask server:

``` bash
flask run
```

3.  When prompted for a password, insert your MySQL/MariaDB root
    password

The database will be created and populated automatically, if needed,
each time the server is executed

4.  The website can be accessed via **localhost:5000**
