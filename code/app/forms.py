from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, SelectField, StringField, SubmitField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")


class AnnualForm(FlaskForm):
    year_list = [
        ("2011", 2011),
        ("2012", 2012),
        ("2013", 2013),
        ("2014", 2014),
        ("2015", 2015),
        ("2016", 2016),
        ("2017", 2017),
        ("2018", 2018),
    ]
    name = StringField("Search by Glacier Name")
    year = SelectField("Search by Year", validators=[DataRequired()], choices=year_list)
    submit = SubmitField("Search")


class PlotForm(FlaskForm):
    name = StringField("Glacier Name", validators=[DataRequired()])
    submit = SubmitField("Search")
