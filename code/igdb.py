from app import app
from database import db_setup, export, parser
from processing import dataframe

db_setup.main()
parser.main()
export.main()
