## Source files
## (Adjust to your needs. Order of markdown files in $(SRC) matters!)
PANDOC 									=	pandoc
DOC 										= docs
META                    = assets/metadata.yaml
SRC                     = $(DOC)/Project.md
BIBFILE                 = assets/Citations.bib
TARGET                  = $(DOC)/Project.pdf



###############################################################################
## Internal setup (do not change)
###############################################################################


## Auxiliary files
## (Do not change!)
TITLEPAGE               = titlepage.tex
TMP                     = assets/$(TITLEPAGE:%.tex=__%.filled.tex)


## Pandoc options
AUX_OPTS                =

OPTIONS                 = -f markdown
OPTIONS                += --pdf-engine=pdflatex
OPTIONS                += --filter=pandoc-citeproc
OPTIONS                += --metadata-file=$(META)
OPTIONS                += -M bibliography=$(BIBFILE)
OPTIONS                += --listings
OPTIONS                += --include-in-header=$(TMP)
OPTIONS                += --resource-path=./assets/Diagrams


## Template variables
TEMPLATE_DL_DIR         = .tmp_template_dl

CLEANTHESIS_TEMPLATE    = assets/cleanthesis.sty

TEMPLATE_FILES          = $(CLEANTHESIS_TEMPLATE)

###############################################################################
## Main targets (do not change)
###############################################################################

$(SRC): $(DOC)/Project.org
	$(PANDOC) -o $@ $<

## Use Clean Thesis template (https://github.com/derric/cleanthesis)
cleanthesis: TEMPLATE_FILE    += $(CLEANTHESIS_TEMPLATE)
cleanthesis: TEMPLATE_REPO    += $(CLEANTHESIS_REPO)
cleanthesis: TEMPLATE_VERSION += $(CLEANTHESIS_VERSION)
cleanthesis: AUX_OPTS         += -M cleanthesis=true -M cleanthesisbibfile=$(BIBFILE:%.bib=%)
cleanthesis: OPTIONS          += --include-in-header=assets/include-header.tex $(AUX_OPTS)
cleanthesis: $(CLEANTHESIS_TEMPLATE) $(TARGET)


## Clean-up: Remove temporary (generated) files and download folder
clean:
	rm -rf $(TMP) $(TEMPLATE_DL_DIR)


## Clean-up: Remove also genereated thesis and template files
distclean: clean
	rm -f $(TARGET)



###############################################################################
## Auxiliary targets (do not change)
###############################################################################


## Download template files

## Build thesis
${TARGET}: $(SRC) $(META) $(BIBFILE) $(TMP)
	$(PANDOC) ${OPTIONS} -o $@ $(SRC)


## Build auxiliary files (title page, references)
$(TMP): assets/__%.filled.tex: assets/%.tex $(META)
	$(PANDOC) $(AUX_OPTS) --template=$< --metadata-file=$(META) -o $@ $<


###############################################################################
## Declaration of phony targets
###############################################################################


.PHONY: cleanthesis clean distclean
