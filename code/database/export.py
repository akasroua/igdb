from app import db
from app.models import Annual_Data, Glacier, User
from pandas import DataFrame, read_csv


def create_dataframes() -> {DataFrame}:
    files = {
        "glacier": "../data/glacier.csv",
        "annual_data": "../data/annual_data.csv",
        "user": "../data/user.csv",
    }
    df_list = {}
    for csv in files.keys():
        df_list[csv] = read_csv(files[csv])
    return df_list


def insert_data(df_list):
    models = [Glacier, Annual_Data, User]
    for model in models:
        if model.query.first() is not None:
            return
    for key, value in df_list.items():
        value.to_sql(key, con=db.engine, index=False, if_exists="append")


def main():
    df_list = create_dataframes()
    insert_data(df_list)


if __name__ == "__main__":
    main()
