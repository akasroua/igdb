from database.constants import CONNECTION_URI


class Config(object):
    SQLALCHEMY_DATABASE_URI = CONNECTION_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "trolaso"
