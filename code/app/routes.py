from app import app
from app.forms import AnnualForm, LoginForm, PlotForm
from database.queries import query_annual_data, query_plot_data, query_user
from flask import flash, redirect, render_template, request, url_for, send_file
from flask_login import current_user, login_required, login_user, logout_user
from processing.dataframe import create_table, create_plot
from werkzeug.urls import url_parse


@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html", title="Home Page")


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("admin"))
    form = LoginForm()
    if form.validate_on_submit():
        user = query_user(form)
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("admin")
        return redirect(next_page)
    return render_template("login.html", title="Sign In", form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))


@app.route("/admin")
@login_required
def admin():
    return render_template("admin.html", title="Admin Page")


@app.route("/nuke")
@login_required
def nuke():
    return render_template("nuked.html", title="NUKE THE SYSTEM!")


@app.route("/data")
def data():
    return render_template("data.html", title="Data")


@app.route("/table_selection", methods=["GET", "POST"])
def table_selection():
    form = AnnualForm()
    if form.validate_on_submit():
        query = query_annual_data(form)
        table = create_table(query.statement)
        return render_template("table.html", table=table, title="Table")
    return render_template("table_selection.html", title="Data", form=form)


@app.route("/table")
def table():
    return render_template("table.html", table=table, title="Table")


@app.route("/plot_selection", methods=["GET", "POST"])
def plot_selection():
    form = PlotForm()
    if form.validate_on_submit():
        query = query_plot_data(form)
        plot = create_plot(query.statement)
        return render_template("plot.html", title="Plot", plot=plot)
    return render_template("plot_selection.html", title="Data", form=form)


@app.route("/plot")
def plot():
    return render_template("plot.html", title="Plot", plot=plot)


@app.route("/figure")
def figure(query):
    fig = create_plot(query)
    return send_file(fig, mimetype="image/png")
