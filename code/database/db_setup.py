from app import db
from database.constants import DB_NAME, DB_USER, DB_PW
from subprocess import run


def create_database():
    script = "database/mariadb_setup.sh"
    output = run([script, DB_NAME, DB_USER, DB_PW])
    if output.returncode != 0:
        print("Error: couldn't create database")
        exit()


def create_tables():
    db.create_all()


def main():
    create_database()
    create_tables()


if __name__ == "__main__":
    main()
